const fs = require('fs').promises;
const path = require('path');

class Util {

    /**
    * Remove directory recursively
    * @param {string} dirPath
    */
    static async rimraf(dirPath) {

    }

    /**
    * Copy directory recursively
    * @param {string} srcPath
    * @param {string} destPath
    */
    static async cpdir(srcPath, destPath) {

        let stats = await fs.lstat(srcPath);
        console.log(stats.isDirctory());

    }

}

module.exports.Util