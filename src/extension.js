// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const fs = require('fs')
const fsp = fs.promises;
const path = require('path');
const cp = require('child_process');
const os = require('os');

const Util = require('./Util');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed



let outc = vscode.window.createOutputChannel('pipenv_install');



/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Extension esp32-micropython is now active.');

	const storagePath = context.globalStoragePath;
	const storagePathScript = path.join(storagePath, 'serialport.py');
	const modulePath = __dirname;
	const modulePathScript = path.join(__dirname, 'serialport.py');


	function showPaths() {
		console.log(modulePath);
		console.log(modulePathScript);
		console.log(storagePath);
		console.log(storagePathScript);
	}

	async function checkDependencies() {
		return new Promise((resolve, reject) => {
			resolve('checkDependencies done');
		});
	}

	async function cleanUp() {
		console.log('running cleanUp');

		let dirExisted = false;

		await fsp.mkdir(storagePath).catch((error) => {
			if (error.code === 'EEXIST') {
				dirExisted = true;
			}

		});

		if (dirExisted) {
			console.log('delete old dir rec.');
			console.log('copy project rec.');
		} else {
			console.log('copy project rec.');
		}

		console.log(dirExisted);

		return ('DONE');


	}

	function copyPythonProject() {
		console.log('running copyPythonProject');
	}

	function runPipenvInstall() {
		console.log('running runPipenvInstall');
		let term = vscode.window.createTerminal({
			name: 'pipenv install'
		});
		term.show();
	}

	let rebuildInfrastructure = vscode.commands.registerCommand('extension.rebuildInfrastructure', () => {
		showPaths();

		checkDependencies()
			.then(status => {
				console.log(status);
				return cleanUp();
			})
			.then(status => {
				console.log(status)
			});
		copyPythonProject();
		runPipenvInstall();
	});

	context.subscriptions.push(rebuildInfrastructure);


	// let choosePortCommand = vscode.commands.registerCommand('extension.choosePort', () => {

	// 	outc.show();

	// 	const ls = cp.spawn('pipenv', ['--user', 'install'], {
	// 		cwd: storagePath,
	// 		detached: false,
	// 		shell: false,
	// 		env: {
	// 			'LC_ALL': 'C.UTF-8',
	// 			'LANG': 'C.UTF-8',
	// 			'PIPENV_HIDE_EMOJIS': '1'
	// 		}
	// 	});

	// 	ls.stdout.setEncoding('utf8');
	// 	ls.stderr.setEncoding('utf8');

	// 	ls.stdout.on('data', (data) => {
	// 		if(data.includes(os.EOL)) {
	// 			let pos = data.indexOf(os.EOL);
	// 			lineOut += data.substring(0, pos);
	// 			outc.appendLine(lineOut);
	// 			lineOut = data.substring(pos+1); 
	// 		} else {
	// 			lineOut += data;
	// 		}
	// 	});

	// 	ls.stderr.on('data', (data) => {
	// 		if(data.includes(os.EOL)) {
	// 			let pos = data.indexOf(os.EOL);
	// 			lineError += data.substring(0, pos);
	// 			outc.appendLine(lineError);
	// 			lineError = data.substring(pos+1); 
	// 		} else {
	// 			lineError += data;
	// 		}
	// 	});

	// 	ls.on('close', (code) => {
	// 		outc.append(`child process exited with code ${code}`);
	// 		vscode.window.showInformationMessage('pipenv install ended successfully');
	// 	});

	// 	// console.log('executing pipenv run serialport');
	// 	// command = 'pipenv run serialport';
	// 	// options = {
	// 	// 	cwd: storagePath
	// 	// }

	// 	// cp.exec(command, options, (err, stdout, stderr) => {
	// 	// 	console.log(stdout);
	// 	// 	console.error(stderr);
	// 	// 	if (err) {
	// 	// 		console.error('error: ' + err);
	// 	// 	}
	// 	// });

	// });

	// context.subscriptions.push(choosePortCommand);
}

// this method is called when your extension is deactivated
function deactivate() {
	console.log('Extension deactivated.');
}

module.exports = {
	activate,
	deactivate
}
